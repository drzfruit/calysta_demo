-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 11 Jul 2019 pada 11.47
-- Versi server: 5.6.14
-- Versi PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `calysta_demo`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `a_kantor`
--

CREATE TABLE `a_kantor` (
  `id` int(4) NOT NULL,
  `kode` varchar(2) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `a_kantor`
--

INSERT INTO `a_kantor` (`id`, `kode`, `nama`, `alamat`) VALUES
(1, 'PS', 'Pusat', 'Margacinta'),
(2, 'KP', 'Kopo', 'Kopo');

-- --------------------------------------------------------

--
-- Struktur dari tabel `b_karyawan`
--

CREATE TABLE `b_karyawan` (
  `id` int(11) NOT NULL,
  `a_kantor_id` int(11) DEFAULT NULL,
  `username` varchar(24) NOT NULL,
  `password` varchar(78) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `b_karyawan`
--

INSERT INTO `b_karyawan` (`id`, `a_kantor_id`, `username`, `password`) VALUES
(1, NULL, 'deadmin', 'e10adc3949ba59abbe56e057f20f883e'),
(2, 1, 'admpusat', 'e10adc3949ba59abbe56e057f20f883e'),
(3, 2, 'admkopo', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Struktur dari tabel `c_produk`
--

CREATE TABLE `c_produk` (
  `id` int(11) NOT NULL,
  `a_kantor_id` int(11) DEFAULT NULL,
  `jenis` varchar(24) NOT NULL,
  `kategori` varchar(24) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `harga` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `c_produk`
--

INSERT INTO `c_produk` (`id`, `a_kantor_id`, `jenis`, `kategori`, `nama`, `harga`) VALUES
(1, NULL, 'Barang', 'Krim Malam', 'NCF1', 70000),
(2, NULL, 'Jasa', 'Facial', 'Facial Basic', 100000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `d_stok`
--

CREATE TABLE `d_stok` (
  `id` int(11) NOT NULL,
  `a_kantor_id_asal` int(11) DEFAULT NULL,
  `a_kantor_id_tujuan` int(11) DEFAULT NULL,
  `c_produk_id` int(11) NOT NULL,
  `stok_masuk` int(4) NOT NULL,
  `stok_keluar` int(4) NOT NULL,
  `stok_asal` int(8) NOT NULL,
  `stok` int(8) NOT NULL,
  `tgl_dibuat` datetime NOT NULL,
  `tgl_diubah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `d_stok`
--

INSERT INTO `d_stok` (`id`, `a_kantor_id_asal`, `a_kantor_id_tujuan`, `c_produk_id`, `stok_masuk`, `stok_keluar`, `stok_asal`, `stok`, `tgl_dibuat`, `tgl_diubah`) VALUES
(1, NULL, 1, 1, 10, 0, 0, 10, '2019-07-10 00:00:00', '2019-07-10 06:54:57'),
(2, NULL, 1, 1, 10, 0, 10, 20, '2019-07-10 00:00:00', '2019-07-10 06:55:48'),
(3, 1, 2, 1, 0, 5, 20, 15, '2019-07-10 00:00:00', '2019-07-10 06:57:00');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `a_kantor`
--
ALTER TABLE `a_kantor`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `b_karyawan`
--
ALTER TABLE `b_karyawan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `a_kantor_id` (`a_kantor_id`);

--
-- Indeks untuk tabel `c_produk`
--
ALTER TABLE `c_produk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `a_kantor_id` (`a_kantor_id`);

--
-- Indeks untuk tabel `d_stok`
--
ALTER TABLE `d_stok`
  ADD PRIMARY KEY (`id`),
  ADD KEY `a_kantor_id_asal` (`a_kantor_id_asal`),
  ADD KEY `a_kantor_id_tujuan` (`a_kantor_id_tujuan`),
  ADD KEY `c_produk_id` (`c_produk_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `a_kantor`
--
ALTER TABLE `a_kantor`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `b_karyawan`
--
ALTER TABLE `b_karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `c_produk`
--
ALTER TABLE `c_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `d_stok`
--
ALTER TABLE `d_stok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `b_karyawan`
--
ALTER TABLE `b_karyawan`
  ADD CONSTRAINT `b_karyawan_ibfk_1` FOREIGN KEY (`a_kantor_id`) REFERENCES `a_kantor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
