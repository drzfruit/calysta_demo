<?php
class c_produk extends SENE_Model{
  var $tbl = 'c_produk';
  var $tbl_as = 'cp';

  public function __construct(){
    parent::__construct();
  }
  public function getAll(){
    $this->db->select('*')->from($this->tbl);
    return $this->db->get("object",0);
  }
  public function getById($nip){
    $this->db->where("id",$nip);
    $this->db->from($this->tbl,$this->tbl_as);
    return $this->db->get_first();
  }
  public function insert($dataArray=array()){
    return $this->db->insert($this->tbl,$dataArray);
  }
  public function update($nim,$dataUpdate=array()){
    $this->db->where("id",$nim);
    return $this->db->update($this->tbl,$dataUpdate);
  }
  public function delete($nim){
    $this->db->where("id",$nim);
    return $this->db->delete($this->tbl);
  }
}
