<?php
class B_Karyawan_Model extends SENE_Model{
  var $tbl = 'b_karyawan';
  var $tbl_as = 'bkm  ';

  public function __construct(){
    parent::__construct();
  }
  public function getAll(){
    $this->db->select('*')->from($this->tbl);
    return $this->db->get("object",0);
  }
  public function getById($id){
    $this->db->where("id",$id);
    $this->db->from($this->tbl,$this->tbl_as);
    return $this->db->get_first();
  }
  public function insert($dataArray=array()){
    return $this->db->insert($this->tbl,$dataArray);
  }
  public function update($id,$dataUpdate=array()){
    $this->db->where("id",$id);
    return $this->db->update($this->tbl,$dataUpdate);
  }
  public function delete($id){
    $this->db->where("id",$id);
    return $this->db->delete($this->tbl);
  }
  public function auth($username, $password){
    $this->db->from($this->tbl,$this->tbl_as);
    $this->db->where_as("username",$this->db->esc($username));
    $this->db->where_as("password",$this->db->esc($password));
    return $this->db->get_first();
  }
}
