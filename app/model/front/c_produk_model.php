<?php
class C_Produk_Model extends SENE_Model{
  var $tbl = 'c_produk';
  var $tbl_as = 'cp';

	public function __construct(){
    parent::__construct();
  }
	public function getAll(){
		$this->db->select('*')->from($this->tbl);
    return $this->db->get("object",0); //array of (object) or (array) of array, second debug true or false
	}
	public function insert($dataArray=array()){
		//make sure your data array key match with column name in table
		return $this->db->insert($this->tbl,$dataArray); //return true if sucess or false if fail
	}
	public function update($id,$dataUpdate=array()){
		//make sure your data array key match with column name in table
    $this->db->where("id",$id);
    return $this->db->update($this->tbl,$dataUpdate); //return true if sucess or false if fail
	}
	public function delete($id){
		$this->db->where("id",$id);
		return $this->db->delete($this->tbl);//return true or false
	}
  public function getById($id){
    $this->db->where("id",$id);
    $this->db->from($this->tbl,$this->tbl_as);
    return $this->db->get_first();
  }
}
