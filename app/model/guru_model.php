<?php
class Guru_Model extends SENE_Model{
  var $tbl = 'guru';
  var $tbl_as = 'g';

  public function __construct(){
    parent::__construct();
  }
  public function getAll(){
    $this->db->select('*')->from($this->tbl);
    return $this->db->get("object",0);
  }
  public function insert($dataArray=array()){
    return $this->db->insert($this->tbl,$dataArray);
  }
  public function update($nip,$dataUpdate=array()){
    $this->db->where("nip",$nip);
    return $this->db->update($this->tbl,$dataUpdate);
  }
  public function delete($nip){
    $this->db->where("nip",$nip);
    return $this->db->delete($this->tbl);
  }
  public function getById($nip){
    $this->db->where("nip",$nip);
    $this->db->from($this->tbl,$this->tbl_as);
    return $this->db->get_first();
  }
}
