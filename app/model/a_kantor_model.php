<?php
class Guru_Model extends SENE_Model{
  var $tbl = 'guru';
  var $tbl_as = 'g';

	public function __construct(){
    parent::__construct();
  }
	public function getAll(){
		$this->db->select('*')->from($this->tbl);
    return $this->db->get("object",0); //array of (object) or (array) of array, second debug true or false
	}
  public function getById($nip){
    $this->db->where("nip",$nip);
    $this->db->from($this->tbl,$this->tbl_as);
    return $this->db->get_first();
  }
	public function insert($dataArray=array()){
		//make sure your data array key match with column name in table
		return $this->db->insert($this->tbl,$dataArray); //return true if sucess or false if fail
	}
	public function update($nip,$dataUpdate=array()){
		//make sure your data array key match with column name in table
    $this->db->where("nip",$nip);
    return $this->db->update($this->tbl,$dataUpdate); //return true if sucess or false if fail
	}
	public function delete($nip){
		$this->db->where("nip",$nip);
		return $this->db->delete($this->tbl);//return true or false
	}
}
