<?php
class ulangan_Model extends SENE_Model{
  var $tbl = 'ulangan';
  var $tbl_as = 'u';
  var $tbl2 = 'guru';
  var $tbl2_as = 'g';
  var $tbl3 = 'siswa';
  var $tbl3_as = 's';
  public function __construct(){
    parent::__construct();
  }
  public function getAll(){
    $this->db->select_as("$this->tbl_as.id","id",0);
    $this->db->select_as("$this->tbl2_as.nama","guru",0);
    $this->db->select_as("$this->tbl2_as.pelajaran","pelajaran",0);
    $this->db->select_as("$this->tbl3_as.nama","siswa",0);
    $this->db->select_as("$this->tbl_as.nilai","nilai",0);

    $this->db->from($this->tbl,$this->tbl_as);
    $this->db->join($this->tbl2,$this->tbl2_as,'nip',$this->tbl_as,'guru_nip','');
    $this->db->join($this->tbl3,$this->tbl3_as,'nim',$this->tbl_as,'siswa_nim','');
    return $this->db->get("object",0);
  }
  public function insert($dataArray=array()){
    return $this->db->insert($this->tbl,$dataArray);
  }
  public function update($id,$dataUpdate=array()){
    $this->db->where("id",$id);
    return $this->db->update($this->tbl,$dataUpdate);
  }
  public function delete($id){
    $this->db->where("id",$id);
    return $this->db->delete($this->tbl);
  }
  public function getById($id){
    $this->db->from($this->tbl,$this->tbl_as);
    $this->db->where("id",$id);
    return $this->db->get_first();
  }
}
