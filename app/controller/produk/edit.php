<?php
class Edit extends JI_controller{

public function __construct(){
  parent::__construct();
  $this->setTheme('front');
  $this->load("front/c_produk_model","cpm");
}
public function index($id=""){
  $data = $this->__init();
  $jenis = $this->input->post("jenis");
  $kategori = $this->input->post("kategori");
  $nama=$this->input->post("nama");
  $harga=$this->input->post("harga");
  if(empty($nama)) $nama = '';
  if(strlen($nama)>1){
    $du = array();
    $du['jenis'] = $jenis;
    $du['kategori'] = $kategori;
    $du['nama']= $nama;
    $du['harga']= $harga;
    $res = $this->cpm->update($id,$du);
    if($res){
      $data['notif'] = 'Data produk berhasil diubah';
  }
}
$data['produk'] = $this->cpm->getById($id);
$data['brand'] = $this->site_name;
$data['page_current'] = 'produk';
$this->setTitle('Data produk');
$this->setDescription('SEME Framework PHP MVC Framework with small footprint for your business.');
$this->setKeyword('SEME Framework');
$this->putThemeLeftContent("page/html/sidebar_left",$data);
$this->putThemecontent("produk/edit",$data);
$this->putJsContent("produk/edit_bottom",$data);
$this->loadLayout("col-2-left",$data);
$this->render();
}
}
