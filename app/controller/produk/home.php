<?php
class Home extends JI_Controller{

	public function __construct(){
    parent::__construct();
		$this->setTheme('front');
		$this->load("front/c_produk_model","cpm");
	}
	public function index(){
		$data = $this->__init(); //method from app/core/ji_controller

		$data['produk'] = $this->cpm->getAll();

		//for menu top bar
		$data['brand'] = $this->site_name;

		//example data passing


		//for set pills active
		$data['page_current'] = 'produk';

		//this config can be found on app/view/front/page/html/head.php
		$this->setTitle('Data produk');
		$this->setDescription('SEME Framework PHP MVC Framework with small footprint for your business.');
		$this->setKeyword('SEME Framework');

		//sidebar left
		//this view can be found on app/view/front/page/html/sidebar_left.php
		$this->putThemeLeftContent("page/html/sidebar_left",$data); //pass data to view

		//sidebar right
		//this view can be found on app/view/front/page/html/sidebar_left.php
		$this->putThemeRightContent("page/html/sidebar_right",$data); //pass data to view

		//main content
		//this view can be found on app/view/front/home/home.php
		$this->putThemeContent("produk/home",$data); //pass data to view
		//this view for INPAGE JS Script can be found on app/view/front/page/home/home_bottom.php
		$this->putJsContent("produk/home_bottom",$data); //pass data to view

		$this->loadLayout("col-2-left",$data);
		$this->render();
	}

}
