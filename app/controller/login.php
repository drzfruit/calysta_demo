<?php
class login extends JI_Controller{
	public function __construct(){
    parent::__construct();
		$this->setTheme('front');
    $this->load("front/b_karyawan_model","bkm");
	}
	public function index(){
		$data = $this->__init(); //method from app/core/ji_controller
		//for menu top bar
		if (isset($data['sess']->user->id)){
			redir(base_url());
			die();
		}
		$data['brand'] = $this->site_name;
		//this config can be found on app/view/front/page/html/head.php
		$this->setTitle('Welcome to SEME Framework');
		$this->setDescription('SEME Framework PHP MVC Framework with small footprint for your business.');
		$this->setKeyword('SEME Framework');
		//main content
		$this->putThemeContent("login/home",$data); //pass data to view
		$this->loadLayout("col-1",$data);
		$this->render();
	}
  public function proses(){
    $username = $this->input->post("username");
    $password = $this->input->post("password");
    $password = md5($password);
    $karyawan = $this->bkm->auth($username,$password);
    if(isset($karyawan->id)){
			$data = $this->__init();
			if(!isset($data['sess'])) $data['sess'] = new stdClass();
			if(!isset($data['sess']->user)) $data['sess']->user = new stdClass();
			$data['sess']->user = $karyawan;
			$this->setkey($data['sess']);
			redir(base_url());
			die();
    }else{
      redir(base_url("login"));
			die();
    }
  }
}
