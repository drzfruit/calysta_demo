<?php
class API extends JI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load("front/c_produk_model","cpm");
  }
  public function index(){
    $data = array();
    $this->status = 404;
    $this->message = 'not found';
    $this->__json_out($data);
  }
  public function hapus($id){
    $data = array();
    $this->status = 404;
    $this->message = 'not found';
    $id = (int) $id;
    if($id>0){
      $res = $this->cpm->delete($id);
      if($res){
        $this->status = 200;
        $this->message = 'berhasil';
      }else{
        $this->status = 900;
        $this->message = 'Gagal menghapus dari data base';
      }
    }else{
      $this->status = 700;
      $this->message = 'produk ID Tidak valid';
    }
    $this->__json_out($data);
  }
}
