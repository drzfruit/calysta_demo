<?php
class Tambah extends JI_controller{

public function __construct(){
  parent::__construct();
  $this->setTheme('front');
  $this->load("front/d_stok_model","dsm");
}
public function index($id=""){
  $data = $this->__init();
  $jenis = $this->input->post("jenis");
  $kategori = $this->input->post("kategori");
  $nama = $this->input->post("nama");
  $harga = $this->input->post("harga");
  if(empty($nama)) $nama = '';
  if(strlen($nama)>1){
    $di = array();
    $di['jenis'] = $jenis;
    $di['kategori'] = $kategori;
    $di['nama'] = $nama;
    $di['harga'] = $harga;
    $res = $this->cpm->insert($di);
    if($res){
      $data['notif'] = 'Data produk berhasil diubah';
  }
}
$data['brand'] = $this->site_name;
$data['page_current'] = 'produk';
$this->setTitle('Tambah Data produk');
$this->setDescription('SEME Framework PHP MVC Framework with small footprint for your business.');
$this->setKeyword('SEME Framework');
$this->putThemeLeftContent("page/html/sidebar_left",$data);
$this->putThemeContent("produk/tambah",$data);
$this->loadLayout("col-2-left",$data);
$this->render();
}
}
