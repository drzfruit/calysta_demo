<?php
class logout extends JI_Controller{
	public function __construct(){
    parent::__construct();
		$this->setTheme('front');
    $this->load("front/b_karyawan_model","bkm");
	}
	public function index(){
		$data = $this->__init();
    if(!isset($data['sess'])) $data['sess'] = new stdClass();
    if(!isset($data['sess']->user)) $data['sess']->user = new stdClass();
    $data['sess']->user = new stdClass();
    $this->setkey($data['sess']);
    redir(base_url("login"));
    die();
  }
}
