<?php
class Edit extends JI_controller{

public function __construct(){
  parent::__construct();
  $this->setTheme('front');
  $this->load("front/a_kantor_model","akm");
  $this->load("front/b_karyawan_model","bkm");
}
public function index($id=""){
  $data = $this->__init();
  $a_kantor_id = $this->input->post("a_kantor_id");
  $username=$this->input->post("username");
  $password = $this->input->post("password");
  if(empty($username)) $username = '';
  if(strlen($username)>1){
    $du = array();
    $du['id'] = $id;
    $du['a_kantor_id'] = $a_kantor_id;
    $du['username']= $username;
    $du['password']=$password;
    $res = $this->bkm->update($id,$du);
    if($res){
      $data['notif'] = 'Data Kantor berhasil diubah';
  }
}
$data['karyawan'] = $this->bkm->getById($id);
$data['kantor']= $this->akm->getAll();
$data['brand'] = $this->site_name;
$data['page_current'] = 'kantor';
$this->setTitle('Data Kantor');
$this->setDescription('SEME Framework PHP MVC Framework with small footprint for your business.');
$this->setKeyword('SEME Framework');
$this->putThemeLeftContent("page/html/sidebar_left",$data);
$this->putThemecontent("karyawan/edit",$data);
$this->putJsContent("karyawan/edit_bottom",$data);
$this->putThemeContent("karyawan/edit",$data);
$this->putJsContent("karyawan/edit_bottom",$data);
$this->loadLayout("col-2-left",$data);
$this->render();
}
}
