<?php
class API extends JI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load("front/b_karyawan_model","bkm");
  }
  public function index(){
    $data = array();
    $data['karyawan']= new stdClass();
    $res = $this->bkm->getAll();
    if($res){
      $this->status=200;
      $this->message = 'Berhasil';
    }else{
      $this->status = 404;
      $this->message = 'not found';
    }
    $data['karyawan']=$res;

    $this->__json_out($data);
  }
  public function hapus($id){
    $data = array();
    $this->status = 404;
    $this->message = 'not found';
    $id = (int) $id;
    if($id>0){
      $res = $this->cpm->delete($id);
      if($res){
        $this->status = 200;
        $this->message = 'berhasil';
      }else{
        $this->status = 900;
        $this->message = 'Gagal menghapus dari data base';
      }
    }else{
      $this->status = 700;
      $this->message = 'karyawan ID Tidak valid';
    }
    $this->__json_out($data);
  }
}
