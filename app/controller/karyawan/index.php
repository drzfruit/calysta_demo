<?php
$nama = 'Ruli';
$perusahaan='Pom Mini Ruli';
include("_koneksi.php");
?>
<!doctype html>
<html>
<?php include('_head.php'); ?>
<body>

  <?php include("_navbar.php"); ?>
  <div class="container">
    <div class="row">
      <?php include("_menu.php"); ?>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-12">
            <h1><?php echo $nama; ?> Pake Bootstrap Bro!</h1>
            <div class="col-md-3">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nama</th>
                  <th>pelajaran</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $query = 'SELECT * FROM guru WHERE 1';
                $result = mysqli_query($link,$query);
                foreach(mysqli_fetch_all($result,MYSQLI_ASSOC) as $row){
                  ?>
                <tr>
                  <td><?=$row['nip']?></td>
                  <td><?=$row['nama']?></td>
                  <td><?=$row['pelajaran']?></td>
                  <td>
                    <a href="guru_edit.php?nip=<?=$row['nip']?>">edit</a>
                    <a href="guru_hapus.php?nip=<?=$row['nip']?>">hapus</a>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>

        </div>


      </div>
    </div>
  </div>

  <?php include('_script.php'); ?>
</body>
</html>
