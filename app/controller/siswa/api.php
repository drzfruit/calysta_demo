<?php
class API extends JI_Controller{
	public function __construct(){
    parent::__construct();
		$this->load("siswa_model");
	}
	public function index(){
    $data = array();
    $this->status = 404;
    $this->message = 'Not found';
    $this->__json_out($data);
	}
	public function hapus($siswa_id){
    $data = array();
    $this->status = 404;
    $this->message = 'Not found';
    $siswa_id = (int) $siswa_id;
    if($siswa_id>0){
      $res = $this->siswa_model->delete($siswa_id);
      if($res){
        $this->status = 200;
        $this->message = 'Berhasil';
      }else{
        $this->status = 900;
        $this->message = 'Gagal menghapus dari database';
      }
    }else{
      $this->status = 700;
      $this->message = 'Siswa ID Tidak valid';
    }
    $this->__json_out($data);
	}

}
