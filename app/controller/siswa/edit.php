<?php
class Edit extends JI_Controller{

	public function __construct(){
    parent::__construct();
		$this->setTheme('front');
		$this->load("siswa_model");
	}
	public function index($nip=""){
		$data = $this->__init(); //method from app/core/ji_controller
    $nama = $this->input->post("nama");
    $kelas = $this->input->post("kelas");
    if(empty($nama)) $nama = '';
    if(empty($kelas)) $kelas = '';
    if(strlen($nama)>1){
      $du = array();
      $du['nama'] = $nama;
      $du['kelas'] = $kelas;
      $res = $this->siswa_model->update($nip,$du);
      if($res){
        $data['notif'] = 'Data Siswa berhasil diubah';
      }
    }
		//get data guru from model
		$data['siswa'] = $this->siswa_model->getById($nip);

		//for menu top bar
		$data['brand'] = $this->site_name;

		//example data passing
		$data['example'] = 'Lorem lipsum dolor sit amet. This value can be changed in app/controller/home.php';

		//for set pills active
		$data['page_current'] = 'siswa';

		//this config can be found on app/view/front/page/html/head.php
		$this->setTitle('Data Siswa');
		$this->setDescription('SEME Framework PHP MVC Framework with small footprint for your business.');
		$this->setKeyword('SEME Framework');

		//sidebar left
		//this view can be found on app/view/front/page/html/sidebar_left.php
		$this->putThemeLeftContent("page/html/sidebar_left",$data); //pass data to view

		//sidebar right
		//this view can be found on app/view/front/page/html/sidebar_left.php
		$this->putThemeRightContent("page/html/sidebar_right",$data); //pass data to view

		//main content
		//this view can be found on app/view/front/home/home.php
		$this->putThemeContent("siswa/edit",$data); //pass data to view
		//this view for INPAGE JS Script can be found on app/view/front/page/home/home_bottom.php
		$this->putJsContent("siswa/edit_bottom",$data); //pass data to view

		$this->loadLayout("col-2-left",$data);
		$this->render();
	}

}
