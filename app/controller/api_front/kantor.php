<?php
class Kantor extends JI_Controller{

	public function __construct(){
    parent::__construct();
		//$this->setTheme('frontx');
		$this->load("api_front/a_kantor_model",'akm');
	}
	public function index(){
		$d = $this->__init();
		$data = array();

		$draw = $this->input->post("draw");
		$sval = $this->input->post("search");
		$sSearch = $this->input->request("sSearch");
		$sEcho = $this->input->post("sEcho");
		$page = $this->input->post("iDisplayStart");
		$pagesize = $this->input->request("iDisplayLength");

		$iSortCol_0 = $this->input->post("iSortCol_0");
		$sSortDir_0 = $this->input->post("sSortDir_0");

		$sSortDir_0 = $this->input->post("sSortDir_0");

		$sortCol = "id";
		$sortDir = strtoupper($sSortDir_0);
		if(empty($sortDir)) $sortDir = "DESC";
		if(strtolower($sortDir) != "desc"){
			$sortDir = "ASC";
		}

		switch($iSortCol_0){
			case 0:
				$sortCol = "id";
				break;
			case 1:
				$sortCol = "kode";
				break;
			case 2:
				$sortCol = "nama";
				break;
			case 3:
				$sortCol = "alamat";
				break;
			default:
				$sortCol = "id";
		}

		if(empty($draw)) $draw = 0;
		if(empty($pagesize)) $pagesize=10;
		if(empty($page)) $page=0;
    if(empty($sSearch)) $sSearch = "";
		$keyword = $sSearch;

		$this->status = '100';
		$this->message = 'Berhasil';
		$dcount = $this->akm->countAll($keyword);
		$ddata = $this->akm->getAll($page,$pagesize,$sortCol,$sortDir,$keyword);

		foreach($ddata as &$gd){
			$gd->action_button  = '';
			$gd->action_button .= '<a href="'.base_url("kantor/edit/index/".$gd->id).'" class="btn btn-info"><i class="fa fa-edit"></i> Edit</a>';
			$gd->action_button .= '<a href="'.base_url("kantor/hapus/index/".$gd->id).'" class="btn btn-danger btn-hapus" data-id="<?=$ak->id?>"><i class="fa fa-trash"></i> Hapus</a>';
		}
		//sleep(3);
		$this->__jsonDataTable($ddata,$dcount);
	}
	public function tambah(){
		$d = $this->__init();
		$data = array();
		if(!$this->admin_login){
			$this->status = 400;
			$this->message = 'Harus login';
			header("HTTP/1.0 400 Harus login");
			$this->__json_out($data);
			die();
		}
		$di = $_POST;

		$buat_gudang = 0;
		$clone_produk = 0;
		if(isset($di['buat_gudang'])){
			$buat_gudang = $di['buat_gudang'];
			unset($di['buat_gudang']);
		}
		if(isset($di['clone_produk'])){
			$clone_produk = $di['clone_produk'];
			unset($di['clone_produk']);
		}

		if(!isset($di['kode'])) $di['kode'] = "";
		if(!isset($di['nama'])) $di['nama'] = "";
		if(strlen($di['nama'])>1 && strlen($di['kode'])>1){
			$check = $this->akm->checkKode($di['kode']); //1 = sudah digunakan
			if(empty($check)){
				$res = $this->akm->set($di);
				if($res){
					$a_company_id = $res;
					if(!empty($buat_gudang)){
						$du = array();
						$du['a_company_id'] = $a_company_id;
						$du['utype'] = 'umum';
						$du['kode'] = 'G'.$di['kode'].'00';
						if(strlen($di['nama'])>1){
							$du['kode'] .= substr($di['nama'],0,2);
						}else{
							$du['kode'] .= '00';
						}
						$du['nama'] = 'Gudang Umum '.$di['nama'];
						$du['alamat'] = $di['alamat'];
						$du['provinsi'] = $di['provinsi'];
						$du['kabkota'] = $di['kabkota'];
						$du['kodepos'] = $di['kodepos'];
						$res2 = $this->agm->set($du);
						if($res2){
							$this->status = 100;
							$this->message = 'Data cabang dan data gudang berhasil ditambahkan';
						}else{
							$this->status = 100;
							$this->message = 'Data cabang berhasil, namun data Gudang tidak dapat ditambahkan';
						}
					}else{
						$this->status = 100;
						$this->message = 'Data baru berhasil ditambahkan';
					}
				}else{
					$this->status = 900;
					$this->message = 'Tidak dapat menyimpan data baru, silakan coba beberapa saat lagi';
				}
			}else{
				$this->status = 104;
				$this->message = 'Kode sudah digunakan, silakan coba kode lain';
			}
		}
		$this->__json_out($data);
	}
	public function detail($id){
		$id = (int) $id;
		$d = $this->__init();
		$data = array();
		if(!$this->admin_login && empty($id)){
			$this->status = 400;
			$this->message = 'Harus login';
			header("HTTP/1.0 400 Harus login");
			$this->__json_out($data);
			die();
		}
		$this->status = 100;
		$this->message = 'Berhasil';
		$data = $this->akm->getById($id);
		$this->__json_out($data);
	}
	public function edit(){
		$d = $this->__init();
		$data = array();
		if(!$this->admin_login){
			$this->status = 400;
			$this->message = 'Harus login';
			header("HTTP/1.0 400 Harus login");
			$this->__json_out($data);
			die();
		}
		$du = $_POST;
		if(!isset($du['id'])) $du['id'] = 0;
		$id = (int) $du['id'];
		unset($du['id']);
		if(!isset($du['kode'])) $du['kode'] = "";
		if(!isset($du['nama'])) $di['nama'] = "";
		if($id>0 && strlen($du['nama'])>1 && strlen($du['kode'])>1){
			$check = $this->akm->checkKode($du['kode'],$id); //1 = sudah digunakan
			if(empty($check)){
				$res = $this->akm->update($id,$du);
				if($res){
					$this->status = 100;
					$this->message = 'Perubahan berhasil diterapkan';
				}else{
					$this->status = 901;
					$this->message = 'Tidak dapat melakukan perubahan ke basis data';
				}
			}else{
				$this->status = 104;
				$this->message = 'Kode sudah digunakan, silakan coba kode lain';
			}
		}else{
			$this->status = 444;
			$this->message = 'One or more parameter required';
		}
		$this->__json_out($data);
	}
	public function hapus($id){
		$id = (int) $id;
		$d = $this->__init();
		$data = array();
		if(!$this->admin_login && empty($id)){
			$this->status = 400;
			$this->message = 'Harus login';
			header("HTTP/1.0 400 Harus login");
			$this->__json_out($data);
			die();
		}
		$this->status = 100;
		$this->message = 'Berhasil';
		$res = $this->akm->del($id);
		if(!$res){
			$this->status = 902;
			$this->message = 'Data gagal dihapus';
		}
		$this->__json_out($data);
	}

	public function select2(){
		$d = $this->__init();
		$keyword = $this->input->request('q');
		//die($keyword);
		$ddata = $this->akm->select2($keyword);
		$datares = array();
		$i = 0;
		foreach ($ddata as $key => $value) {
			$datares["results"][$i++] = array("id"=>$value->id,"text"=>$value->kode." - ".$value->nama);
		}
		header('Content-Type: application/json');
		echo json_encode($datares);
	}
}
