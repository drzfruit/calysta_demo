<?php
class Home extends JI_Controller{

	public function __construct(){
    parent::__construct();
		$this->setTheme('front');
	}
	public function index(){
		$data = $this->__init(); //method from app/core/ji_controller
		if(!isset($data['sess']->user->id)){
			redir(base_url("login"));
			die();
		}
		$data['brand'] = $this->site_name;
		$data['page_current'] = 'dashboard';

		$this->setTitle('Welcome to '.$this->site_name);
		$this->setDescription($this->site_description);
		$this->setKeyword($this->site_name);

		$this->putThemeLeftContent("page/html/sidebar_left",$data);
		$this->putThemeContent("home/home",$data); //pass data to view
		$this->putJsContent("home/home_bottom",$data); //pass data to view
		$this->loadLayout("col-2-left",$data);
		$this->render();
	}

}
