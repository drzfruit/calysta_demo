<?php
class Tambah extends JI_controller{

public function __construct(){
  parent::__construct();
  $this->setTheme('front');
  $this->load("front/a_kantor_model","akm");
}
public function index($id=""){
  $data = $this->__init();
  $nama = $this->input->post("nama");
  $kode = $this->input->post("kode");
  $alamat = $this->input->post("alamat");
  if(empty($nama)) $nama = '';
  if(empty($kode)) $kode = '';
  if(strlen($nama)>1){
    $di = array();
    $di['nama'] = $nama;
    $di['kode'] = $kode;
    $di['alamat'] = $alamat;
    $res = $this->akm->insert($di);
    if($res){
      $data['notif'] = 'Data Siswa berhasil diubah';
  }
}
$data['brand'] = $this->site_name;
$data['page_current'] = 'kantor';
$this->setTitle('Tambah Data Kantor');
$this->setDescription('SEME Framework PHP MVC Framework with small footprint for your business.');
$this->setKeyword('SEME Framework');
$this->putThemeLeftContent("page/html/sidebar_left",$data);
$this->putThemeContent("kantor/tambah",$data);
$this->loadLayout("col-2-left",$data);
$this->render();
}
}
