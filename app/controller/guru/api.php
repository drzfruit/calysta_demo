<?php
class API extends JI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load("guru_model","gm");
  }
  public function index(){
    $data = array();
    $data['guru']= new stdClass();
    $res = $this->gm->getAll();
    if($res){
      $this->status=200;
      $this->message = 'Berhasil';
    }else{
      $this->status = 404;
      $this->message = 'not found';
    }
    $data['guru']=$res;

    $this->__json_out($data);
  }
  public function hapus($id){
    $data = array();
    $this->status = 404;
    $this->message = 'not found';
    $id = (int) $id;
    if($id>0){
      $res = $this->gm->delete($id);
      if($res){
        $this->status = 200;
        $this->message = 'berhasil';
      }else{
        $this->status = 900;
        $this->message = 'Gagal menghapus dari data base';
      }
    }else{
      $this->status = 700;
      $this->message = 'Guru ID Tidak valid';
    }
    $this->__json_out($data);
  }
}
