<?php
class Edit extends JI_controller{

public function __construct(){
  parent::__construct();
  $this->setTheme('front');
  $this->load("guru_model");
}
public function index($nip=""){
  $data = $this->__init();
  $nama = $this->input->post("nama");
  $pelajaran = $this->input->post("pelajaran");
  if(empty($nama)) $nama = '';
  if(empty('pelajaran'))$pelajaran = '';
  if(strlen($nama)>1){
    $du = array();
    $du['nama'] = $nama;
    $du['pelajaran'] = $pelajaran;
    $res = $this->guru_model->update($nip,$du);
    if($res){
      $data['notif'] = 'Data Guru berhasil diubah';
  }
}
$data['guru'] = $this->guru_model->getById($nip);
$data['brand'] = $this->site_name;
$data['page_current'] = 'guru';
$this->setTitle('Data Guru');
$this->setTitle('Data Guru');
$this->setDescription('SEME Framework PHP MVC Framework with small footprint for your business.');
$this->setKeyword('SEME Framework');
$this->putThemeLeftContent("page/html/sidebar_left",$data);
$this->putThemecontent("guru/edit",$data);
$this->putJsContent("guru/edit_bottom",$data);
$this->putThemeRightContent("page/html/sidebar_right",$data);
$this->putThemeContent("guru/edit",$data);
$this->putJsContent("guru/edit_bottom",$data);
$this->loadLayout("col-2-left",$data);
$this->render();
}
}
