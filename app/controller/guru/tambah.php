<?php
class Tambah extends JI_controller{

public function __construct(){
  parent::__construct();
  $this->setTheme('front');
  $this->load("guru_model");
}
public function index($nip=""){
  $data = $this->__init();
  $nip = $this->input->post("nip");
  $nama = $this->input->post("nama");
  $pelajaran = $this->input->post("pelajaran");
  if(empty($nip)) $nip = '';
  if(empty($nama)) $nama = '';
  if(empty($pelajaran)) $pelajaran = '';
  if(strlen($nama)>1 && strlen($nama)>1){
    $di = array();
    $di['nip'] = $nip;
    $di['nama'] = $nama;
    $di['pelajaran'] = $pelajaran;
    $res = $this->guru_model->insert($di);
    if($res){
      $data['notif'] = 'Data guru berhasil diubah';
  }
}
$data['brand'] = $this->site_name;
$data['page_current'] = 'guru';
$this->setTitle('Tambah Data Guru');
$this->setDescription('SEME Framework PHP MVC Framework with small footprint for your business.');
$this->setKeyword('SEME Framework');
$this->putThemeLeftContent("page/html/sidebar_left",$data);
$this->putThemecontent("guru/tambah",$data);
$this->putThemeRightContent("page/html/sidebar_right",$data);
$this->putThemeContent("guru/tambah",$data);
$this->loadLayout("col-2-left",$data);
$this->render();
}
}
