<?php
class Edit extends JI_controller{

public function __construct(){
  parent::__construct();
  $this->setTheme('front');
  $this->load("guru_model");
  $this->load("siswa_model");
  $this->load("ulangan_model");
}
public function index($id=""){
  $data = $this->__init();
  $siswa_nim = $this->input->post("siswa_nim");
  $guru_nip = $this->input->post("guru_nip");
  $nilai = $this->input->post("nilai");
  if(empty($siswa_nim)) $siswa_nim = '';
  if(empty($guru_nip)) $guru_nip = '';
  if(empty($nilai)) $nilai = '';
  if(strlen($siswa_nim)>0 && strlen($guru_nip)>0){
    $du = array();
    $du['guru_nip'] = $guru_nip;
    $du['siswa_nim'] = $siswa_nim;
    $du['nilai'] = $nilai;
    $res = $this->ulangan_model->update($id,$du);
    if($res){
      $data['notif'] = 'Data ulangan berhasil diubah';
  }
}
$data['ulangan'] = $this->ulangan_model->getById($id);
$data['guru'] = $this->guru_model->getAll();
$data['siswa'] = $this->siswa_model->getAll();
$data['brand'] = $this->site_name;
$data['example'] = 'Lorem lipsum dolar sit amet. This vakue can be changed in app/controller/home.php';
$data['page_current'] = 'siswa';
$this->setTitle('Data Ulangan');
$this->setTitle('Data Ulangan');
$this->setDescription('SEME Framework PHP MVC Framework with small footprint for your business.');
$this->setKeyword('SEME Framework');
$this->putThemeLeftContent("page/html/sidebar_left",$data);
$this->putThemecontent("ulangan/edit",$data);
$this->putJsContent("ulangan/edit_bottom",$data);
$this->putThemeRightContent("page/html/sidebar_right",$data);
$this->putThemeContent("ulangan/edit",$data);
$this->putJsContent("ulangan/edit_bottom",$data);
$this->loadLayout("col-2-left",$data);
$this->render();
}
}
