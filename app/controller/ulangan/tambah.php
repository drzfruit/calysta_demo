<?php
class Tambah extends JI_controller{

public function __construct(){
  parent::__construct();
  $this->setTheme('front');
  $this->load("guru_model");
  $this->load("siswa_model");
  $this->load("ulangan_model");
}
public function index($nip=""){
  $data = $this->__init();
  $siswa_nim= $this->input->post("siswa_nim");
  $guru_nip= $this->input->post("guru_nip");
  $nilai = $this->input->post("nilai");
  if(empty($siswa_nim)) $nim = '';
  if(empty($guru_nip)) $guru_nip = '';
  if(empty($nilai)) $nilai = '';
  if(strlen($siswa_nim)>0 && strlen($guru_nip)>0){
    $di = array();
    $di['guru_nip'] = $guru_nip;
    $di['siswa_nim'] = $siswa_nim;
    $di['nilai'] = $nilai;
    $res = $this->ulangan_model->insert($di);
    if($res){
      $data['notif'] = 'Data Ulangan berhasil diubah';
  }
}
$data['brand'] = $this->site_name;
$data['guru'] = $this->guru_model->getall();
$data['siswa'] = $this->siswa_model->getALL();
$data['page_current'] = 'siswa';
$this->setTitle('Tambah Data ulangan');
$this->setDescription('SEME Framework PHP MVC Framework with small footprint for your business.');
$this->setKeyword('SEME Framework');
$this->putThemeLeftContent("page/html/sidebar_left",$data);
$this->putThemecontent("ulangan/tambah",$data);
$this->putThemeRightContent("page/html/sidebar_right",$data);
$this->putThemeContent("ulangan/tambah",$data);
$this->loadLayout("col-2-left",$data);
$this->render();
}
}
