<div class="row">
  <div class="col-md-12">
    <h4>Edit Siswa</h4>
    <?php if(isset($notif)){ ?>
      <div class="alert alert-info">
        <p><?=$notif?></p>
      </div>
    <?php } ?>
    <form id="iesiswa_form" action="<?=base_url('siswa/edit/index/'.$siswa->nim)?>" method="post" class="form-horizontal">
      <div class="form-group">
        <div class="col-md-12">
          <label for="ienim">NIM</label>
          <input id="ienim" name="nip" class="form-control disabled" value="<?=$siswa->nim?>" disabled />
        </div>
        <div class="col-md-12">
          <label for="ienama">Nama</label>
          <input id="ienama" name="nama" class="form-control" value="<?=$siswa->nama?>" />
        </div>
        <div class="col-md-12">
          <label for="iekelas">Kelas</label>
          <input id="iekelas" name="kelas" class="form-control" value="<?=$siswa->kelas?>" />
        </div>
      </div>
      <div class="form-action">
        <div class="col-md-12">
          <div class="btn-group pull-right">
            <a href="<?=base_url('guru')?>" class="btn btn-default"><i class="fa fa-chevron-left"></i> Kembali</a>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan Perubahan</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
