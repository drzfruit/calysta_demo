<div class="row">
  <div class="col-md-6">
    <h4>Data Siswa</h4>
  </div>
  <div class="col-md-6">
    <div class="btn-group pull-right">
      <a href="<?=base_url("siswa/tambah/index/")?>" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Data</a>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <table id="siswa_table" class="table">
      <thead>
        <tr>
          <th>NIM</th>
          <th>Nama</th>
          <th>Kelas</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>

        <?php foreach($siswa as $si){ ?>
        <tr>
          <td><?=$si->nim?></td>
          <td><?=$si->nama?></td>
          <td><?=$si->kelas?></td>
          <td>
            <a href="<?=base_url("siswa/edit/index/".$si->nim)?>" class="btn btn-info"><i class="fa fa-edit"></i> Edit</a>
            <a href="<?=base_url("siswa/hapus/index/".$si->nim)?>" class="btn btn-danger btn-hapus" data-id="<?=$si->nim?>"><i class="fa fa-trash"></i> Hapus</a>
          </td>
        </tr>
        <?php } ?>

      </tbody>
    </table>
  </div>
</div>
