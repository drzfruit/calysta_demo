//listener
$("#siswa_table").off("click",".btn-hapus");
$("#siswa_table").on("click",".btn-hapus",function(ev){
  ev.preventDefault();
  var c = confirm("Apakah anda yakin mas bro?");
  if(c){
    var siswa_id = $(this).attr("data-id");
    var apiurl = "<?=base_url()?>siswa/api/hapus/"+encodeURIComponent(siswa_id);
    $.get(apiurl).done(function(dt){
      if(dt.status==200){
        alert("berhasil!");
        window.location.reload();
      }else{
        alert("Gagal: "+dt.message);
      }
    }).fail(function(){
      alert("Error Bray!");
    });
  }
});
