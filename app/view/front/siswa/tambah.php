<div class="row">
  <div class="col-md-12">
    <h4>Tambah Siswa</h4>
    <?php if(isset($notif)){ ?>
      <div class="alert alert-info">
        <p><?=$notif?></p>
      </div>
    <?php } ?>
    <form id="isiswa_form" action="<?=base_url('siswa/tambah/index/')?>" method="post" class="form-horizontal">
      <div class="form-group">
        <div class="col-md-12">
          <label for="inim">NIM</label>
          <input id="inim" name="nim" class="form-control" value="" placeholder="Nomor Induk" />
        </div>
        <div class="col-md-12">
          <label for="inama">Nama</label>
          <input id="inama" name="nama" class="form-control" value="" placeholder="Nama Lengkap" />
        </div>
        <div class="col-md-12">
          <label for="iekelas">Kelas</label>
          <input id="iekelas" name="kelas" class="form-control" value="" placeholder="Kelas" />
        </div>
      </div>
      <div class="form-action">
        <div class="col-md-12">
          <div class="btn-group pull-right">
            <a href="<?=base_url('siswa')?>" class="btn btn-default"><i class="fa fa-chevron-left"></i> Kembali</a>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
