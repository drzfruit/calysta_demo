<div class="row">
  <div class="col-md-12">
    <h4>Tambah karyawan</h4>
    <?php if(isset($notif)){ ?>
      <div class="alert alert-info">
        <p><?=$notif?></p>
      </div>
    <?php } ?>
    <form id="ikaryawan_form" action="<?=base_url('karyawan/tambah/index/')?>" method="post" class="form-horizontal">
      <div class="form-group">
        <div class="col-md-12">
          <label for="ia_kantor_id">a_kantor_id</label>

          <select id="ia_kantor_id" name="a_kantor_id" class="form-control" required>
          <option value="">Pilih kantor</option>
          <?php foreach($kantor as $k){ ?>
          <option value="<?=$k->id?>" ><?=$k->nama?> - <?=$k->id?></option>
        <?php } ?>
      </select>
        </div>
        <div class="col-md-12">
          <label for="ieusername">username</label>
          <input id="ieusername" name="username" class="form-control" value="" placeholder="username" />
        </div>
        <div class="col-md-12">
          <label for="iepassword">Password</label>
          <input id="iepassword" name="password" class="form-control" value="" placeholder="password" />
        </div>
      </div>
      <div class="form-action">
        <div class="col-md-12">
          <div class="btn-group pull-right">
            <a href="<?=base_url('karyawan')?>" class="btn btn-default"><i class="fa fa-chevron-left"></i> Kembali</a>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
