var karyawan_table = {};
var ieid = '';


window.toPlainFloat = function(mny){
	return mny.replace( /^\D+/g, '').split('.').join("");
}

function gritter(isi,jenis='info'){
  $.bootstrapGrowl(isi, {
    type: jenis,
    delay: 2500,
    allow_dismiss: true
  });
};

if(jQuery('#karyawan_table').length>0){
	karyawan_table = jQuery('#karyawan_table')
	.on('preXhr.dt', function ( e, settings, data ){
    NProgress.start();
	}).DataTable({
			"order"					: [[ 0, "desc" ]],
			"responsive"	  : true,
			"bProcessing"		: true,
			"bServerSide"		: true,
			"sAjaxSource"		: "<?=base_url()?>api_front/kantor/",
			"fnServerParams": function ( aoData ) {
				aoData.push(
					{ "name": "a_jenis_id", "value": $("#filter_a_jenis_id").val()},
					{ "name": "b_kategori_id", "value": $("#filter_b_kategori_id").val() },
					{ "name": "utype", "value": $("#filter_utype").val() }
				);
			},
			"fnServerData"	: function (sSource, aoData, fnCallback, oSettings) {
				oSettings.jqXHR = $.ajax({
					dataType 	: 'json',
					method 		: 'POST',
					url 		: sSource,
					data 		: aoData
				}).success(function (response, status, headers, config) {
					console.log(response);
          setTimeout(function(){
            NProgress.done();
            $(".rupiah-uang").priceFormat({
              prefix: 'Rp',
              centsSeparator: ',',
              thousandsSeparator: '.',
              centsLimit: 0
            });

            //edit harga beli
            $(".produk-harga-beli").off("blur");
            $(".produk-harga-beli").on("blur",function(e){
              e.preventDefault();
              var pid = $(this).attr('data-id');
              var url = 'http://calysta-erp.local/api_admin/produk/massupdate/edit/'+pid;
              var fd = {};
              fd.harga_beli = toPlainFloat($(this).val());
              NProgress.start();
              $.post(url,fd).done(function(dt){
                NProgress.done();
                if(dt.status == 100){
                  gritter("<h4>Berhasil</h4><p>Harga beli produk berhasil diubah</p>",'success');
                }else{
                  gritter("<h4>Error</h4><p>"+dt.message+"</p>",'danger');
                }
              }).fail(function(){
                NProgress.done();
                gritter("<h4>Error</h4><p>Terjadi galat pada server</p>",'warning');
              });
            });

            //edit harga jual
            $(".produk-harga-jual").off("blur");
            $(".produk-harga-jual").on("blur",function(e){
              e.preventDefault();
              var pid = $(this).attr('data-id');
              var url = 'http://calysta-erp.local/api_admin/produk/massupdate/edit/'+pid;
              var fd = {};
              fd.harga_jual = toPlainFloat($(this).val());
              NProgress.start();
              $.post(url,fd).done(function(dt){
                NProgress.done();
                if(dt.status == 100){
                  gritter("<h4>Berhasil</h4><p>Harga jual produk berhasil diubah</p>",'success');
                }else{
                  gritter("<h4>Error</h4><p>"+dt.message+"</p>",'danger');
                }
              }).fail(function(){
                NProgress.done();
                gritter("<h4>Error</h4><p>Terjadi galat pada server</p>",'warning');
              });
            });

            //edit visibility
            $(".produk-is-visible").off("change");
            $(".produk-is-visible").on("change",function(e){
              e.preventDefault();
              var pid = $(this).attr('data-id');
              var url = 'http://calysta-erp.local/api_admin/produk/massupdate/edit/'+pid;
              var fd = {};
              fd.is_visible = $(this).val();
              NProgress.start();
              $.post(url,fd).done(function(dt){
                NProgress.done();
                if(dt.status == 100){
                  gritter("<h4>Berhasil</h4><p>Status produk berhasil diubah</p>",'success');
                }else{
                  gritter("<h4>Error</h4><p>"+dt.message+"</p>",'danger');
                }
              }).fail(function(){
                NProgress.done();
                gritter("<h4>Error</h4><p>Terjadi galat pada server</p>",'warning');
              });
            });

            //edit activity
            $(".produk-is-active").off("change");
            $(".produk-is-active").on("change",function(e){
              e.preventDefault();
              var pid = $(this).attr('data-id');
              var url = 'http://calysta-erp.local/api_admin/produk/massupdate/edit/'+pid;
              var fd = {};
              fd.is_active = $(this).val();
              NProgress.start();
              $.post(url,fd).done(function(dt){
                NProgress.done();
                if(dt.status == 100){
                  gritter("<h4>Berhasil</h4><p>Status produk berhasil diubah</p>",'success');
                }else{
                  gritter("<h4>Error</h4><p>"+dt.message+"</p>",'danger');
                }
              }).fail(function(){
                NProgress.done();
                gritter("<h4>Error</h4><p>Terjadi galat pada server</p>",'warning');
              });
            });

          //end timeout
          },1000);
					fnCallback(response);
				}).error(function (response, status, headers, config) {
          NProgress.done();
          gritter("<h4>Error</h4><p>Tidak dapat mengambil data dari server</p>",'warning');
				});
			},
	});
	$('.dataTables_filter input').attr('placeholder', 'Cari');
	$("#afilter_do").on("click",function(e){
		e.preventDefault();
		karyawan_table.ajax.reload(null,false);
	});
}

//listener
$("#karyawan_table").off("click",".btn-hapus");
$("#karyawan_table").on("click",".btn-hapus",function(ev){
  ev.preventDefault();
  var c = confirm("Apakah anda yakin mas bro!");
  if(c){
    var id = $(this).attr("data-id");
    var apiurl = "<?=base_url()?>karyawan/api/hapus/"+encodeURIComponent(id);
    $.get(apiurl).done(function(dt){
      if(dt.status==200){
        alert("Berhasil!");
        window.location.reload();
      }else{
        alert("Gagal: "+dt.message);
      }
    }).fail(function(){
      alert("Error bray!");
    });
  }
});
