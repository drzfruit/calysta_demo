<div class="row">
  <div class="col-md-12">
    <h4>Edit karyawan</h4>
    <?php if(isset($notif)){ ?>
      <div class="alert alert-info">
        <p><?=$notif?></p>
      </div>
    <?php } ?>
    <form id="iekaryawan_form" action="<?=base_url('karyawan/edit/index/'.$karyawan->id)?>" method="post" class="form-horizontal">
      <div class="form-group">
        <div class="col-md-12">
          <label for="ieid">ID</label>
          <input id="id" name="id" class="form-control disabled" value="<?=$karyawan->id?>" disabled />
        </div>
        <div class="col-md-12">
          <label for="ieusername">Username</label>
          <input id="ieusername" name="username" class="form-control" value="<?=$karyawan->username?>" />
        </div>
        <div class="col-md-12">
          <label for="iea_kantor_id">A_kantor_id</label>
          <select id="iea_kantor_id" name="a_kantor_id" class="form-control" required>
          <option value="">Pilih kantor</option>
          <?php foreach($kantor as $k){ ?>
          <option value="<?=$k->id?>" <?php if($k->id == $karyawan->a_kantor_id) echo 'selected'; ?>><?=$k->nama?> - <?=$k->id?></option>
        <?php } ?>
      </select>
        </div>
        <div class="col-md-12">
          <label for="ieid">id</label>
          <input id="ieid" name="id" class="form-control" value="<?=$karyawan->id?>" />
        </div>
        <div class="col-md-12">
          <label for="iepassword">Password</label>
          <input id="iepassword" name="id" class="form-control" value="<?=$karyawan->password?>" />
        </div>
      </div>
      <div class="form-action">
        <div class="col-md-12">
          <div class="btn-group pull-right">
            <a href="<?=base_url('kantor')?>" class="btn btn-default"><i class="fa fa-chevron-left"></i> Kembali</a>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan Perubahan</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
