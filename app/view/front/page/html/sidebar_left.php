<style>
.sidebar-user{
  margin: 20px;
  text-align: right;
}
.gambar-user img{
  width: 64px;
  height: 64px;
  border:2px solid #ffffff;
  border-radius: 50px;
}
.jarak-poto-admin{
  padding: 0px;
}
.nama-user{
  text-align: left;
  padding-top: 20px;
}
.latar-user-admin{
  background: rgba(255, 255, 255, 0.1);
  margin-bottom: 10px;
}
.nama-user a i{
  font-size: 18px;
  color: #9e9e9e;
  margin: 0 5px;
}
</style>

<?php if(!isset($page_current)) $page_current = '';?>

<h4>Left SideBar</h4>

<div class="row latar-user-admin">
  <div class="col-md-6 jarak-poto-admin">
    <div class="sidebar-user">
      <div class="gambar-user">
        <a href="#">
          <img src="<?=base_url("/media/default.png") ?>"alt="">
        </a>
      </div>
    </div>
  </div>
  <div class="col-md-6 jarak-poto-admin">
    <div class="nama-user">
      <h4>Admin calysta</h4>
      <a href="#"><i class="fa fa-user" aria-hidden="true"></i></a>
      <a href="#"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
    </div>
  </div>
</div>


<ul class="nav nav-pills nav-stacked warna-huruf-sidebar">
  <li role="presentation" class="<?php if($page_current == 'dashboard') echo 'active';?>">
    <a href="<?=base_url()?>">Dashboard</a>
  </li>
  <li role="presentation" class="<?php if($page_current == 'kantor') echo 'active';?>">
    <a href="<?=base_url("kantor")?>"><i class="fa fa-file-text-o"></i> Data kantor</a>
  </li>
  <li role="presentation" class="<?php if($page_current == 'karyawan') echo 'active';?>">
    <a href="<?=base_url("karyawan")?>"><i class="fa fa-file-text-o"></i> Data karyawan</a>
  </li>
  <li role="presentation" class="<?php if($page_current == 'produk') echo 'active';?>">
    <a href="<?=base_url("produk")?>"><i class="fa fa-file-text-o"></i> Data produk</a>
  </li>
  <li role="presentation" class="<?php if($page_current == 'stok') echo 'active';?>">
    <a href="<?=base_url("stok")?>"><i class="fa fa-file-text-o"></i> Data stok</a>
  </li>
  <li role="presentatio">
    <a href="<?=base_url("logout")?>"><i class="fa fa-sign-out"></i> Logout</a>
  </li>
</ul>
