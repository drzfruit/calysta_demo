<div class="row">
  <div class="col-md-12">
    <h4>Tambah Siswa</h4>
    <?php if(isset($notif)){ ?>
      <div class="alert alert-info">
        <p><?=$notif?></p>
      </div>
    <?php } ?>
    <form id="iulangan_form" action="<?=base_url('ulangan/tambah/index/')?>" method="post" class="form-horizontal">
      <div class="form-group">
        <div class="col-md-12">
          <label for="iguru_nip">Pilih Guru</label>
          <select id="iguru_nip" name="guru_nip" class="form-control" required>
          <option value="">Pilih Guru</option>
          <?php foreach($guru as $gu){ ?>
          <option value="<?=$gu->nip?>"><?=$gu->nama?> - <?=$gu->pelajaran?></option>
        <?php } ?>
      </select>
        </div>
        <div class="col-md-12">
          <label for="isiswa_nim">Pilih siswa</label>
          <select id="isiswa_nim" name="siswa_nim" class="form-control"required>
            <option value="">Pilih siswa</option>
          <?php foreach($siswa as $si){ ?>
            <option value="<?=$si->nim?>"><?=$si->nama?> - <?=$si->kelas?></option>
          <?php } ?>
        </select>
        </div>
        <div class="col-md-12">
          <label for="inilai">Nilai</label>
          <input id="inilai" name="nilai" class="form-control" value="" placeholder="Nilai Ulangan" />
        </div>
      </div>
      <div class="form-action">
        <div class="col-md-12">
          <div class="btn-group pull-right">
            <a href="<?=base_url('ulangan')?>" class="btn btn-default"><i class="fa fa-chevron-left"></i> Kembali</a>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
