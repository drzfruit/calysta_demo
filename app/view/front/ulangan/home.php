<div class"row">
  <div class="col-md-6">
    <h1>Data Ulangan</h1>
  </div>
  <div class="col-md-6">
    <div class="btn-group pull-right">
      <a href="<?=base_url("ulangan/tambah/index/")?>" class="btn btn-info"><i class="fa fa-plus"></i> tambah</a>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <table id="ulangan_table"class="table">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nama Guru</th>
          <th>Pelajaran</th>
          <th>Siswa_Nim</th>
          <th>Nilai</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>

        <?php foreach($ulangan as $ul){ ?>
          <tr>
            <td><?=$ul->id?></td>
            <td><?=$ul->guru?></td>
            <td><?=$ul->pelajaran?></td>
            <td><?=$ul->siswa?></td>
            <td><?=$ul->nilai?></td>
            <td>
              <a href="<?=base_url("ulangan/edit/index/".$ul->id)?>" class="btn btn-info"><i class="fa fa-edit"></i>Edut<a>
              <a href="#" class="btn btn-danger btn-hapus" data-id="<?=$ul->id?>"><i class="fa fa-trash-o"></i> Hapus</as>
            </td>
          </tr>
        <?php } ?>

      </tbody>
    </table>
  </div>
</div>
