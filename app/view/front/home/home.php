<style>
  .home-posisi-text{
    text-align: center;
    font-size: 35px;
  }
  .home-posisi-text p{
    font-size: 14px;
  }
  .latar-warna-kesatu{
    background: #b1d8bb;
    border-radius: 12px;
  }
  .latar-warna-kedua{
    background:#b9eae8;
    border-radius: 12px;
  }
  .latar-warna-ketiga{
    background:#ffc4c4;
    border-radius:12px;
  }
  .latar-warna-keempat{
    background:#dddddd;
    border-radius:12px;
  }
</style>

<h1><small> Selamat Datang Kembali</small>, <?=$sess->user->username  ?></h1>
<p>Apa Yang Ingin Anda Lakukan?</p>

<div class="row">

  <div class="col-md-3">
    <div class="panel panel-success">
      <div class="panel-body home-posisi-text latar-warna-kesatu">
        <a href="<?=base_url("kantor")?>" class="">
          <i class="fa fa-file-text-o"></i>
          <h4>Data Kantor</h4>
          <p>Kelola Data Kantor Dan Cabang</p>
        </a>
      </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="panel panel-success">
      <div class="panel-body home-posisi-text latar-warna-kedua">
        <a href="<?=base_url("karyawan")?>" class"">
          <i class="fa fa-users" aria-hidden="true"></i>
          <h4>Data Karyawan</h4>
        </a>
      </div>
    </div>
  </div>
    <div class="col-md-3">
      <div class="panel panel-success">
        <div class="panel-body home-posisi-text latar-warna-ketiga">
          <a href="<?=base_url("produk")?>" class"">
            <i class="fa fa-dropbox" aria-hidden="true"></i>
            <h4>Data produk</h4>
          </a>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="panel panel-success">
        <div class="panel-body home-posisi-text latar-warna-keempat">
          <a href="<?=base_url("stok")?>" class"">
            <i class="fa fa-book" aria-hidden="true"></i>
            <h4>Data stok</h4>
          </a>
        </div>
      </div>
    </div>

  </div>
