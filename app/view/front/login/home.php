<style>
  .container{
    width:100%;
    background:#c0c0c0;
  }
  .judul-login{
    background:#ffffff;
    text-align: center;


  }
  .judul-login h2{
  font-size: 26px;
  color: #000000;
  font-weight: bold;
  text-transform: uppercase;
  padding:20px;
  margin-bottom: 0px;
}
.jarak-login{
  margin-top: 150px;
}
.warna-login{
  background: #eaedf1;
}
.form-group{
  padding:10px;
}
.jarak-tombol-login{
  text-align: right;
}
.jarak-kanan-login{
  margin: 0px 20px 20px 0px;
}
</style>

<div class="row">
  <div class="col-md-4">&nbsp;</div>
  <div class="col-md-4 jarak-login">

    <div class="judul-login">
    <h2>Login Calysta</h2>
  </div>

  <div class="warna-login">
    <form id="ilogin"method="post" action="<?=base_url("login/proses")?>" class="form-horizontal">
      <div class="form-group">
        <div class="col-md-12">
          <label for="iusername">iusername</label>
          <input id="iusername" name="username" type="text" class="form-control" required />
        </div>
        <div class="col-md-12">
          <label for="ipassword">password</label>
          <input id="ipassword" name="password" type="password" class="form-control" required />
        </div>
      </div>
      <div class="form-action jarak-tombol-login">
        <div class="btn-group jarak-kanan-login">
          <button type="submit" class="btn btn-success">Login</button>
        </div>
      </div>
    </form>
  </div>


  </div>
  <div class="col-md-4">&nbsp;</div>
</div>
