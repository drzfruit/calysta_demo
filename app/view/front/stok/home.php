<div class="row">
  <div class="col-md-6">
    <h4>Data stok</h4>
  </div>
  <div class="col-md-6">
    <div class="btn-group pull-right">
      <a href="<?=base_url("stok/tambah/index")?>" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Data</a>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <table id="stok_table" class="table">
      <thead>
        <tr>
          <th>Id</th>
          <th>Kantor Asal</th>
          <th>Kantor Tujuan</th>
          <th>stok</th>
          <th>Stok Masuk</th>
          <th>Stok Keluar</th>
          <th>Stok Asal</th>
          <th>Stok</th>
        </tr>
      </thead>
      <tbody>

        <?php foreach($stok as $st){ ?>
          <tr>
            <td><?=$st->id?></td>
            <td><?=$st->kantor_asal?></td>
            <td><?=$st->kantor_tujuan?></td>
            <td><?=$st->produk?></td>
            <td><?=$st->stok_masuk?></td>
            <td><?=$st->stok_keluar?></td>
            <td><?=$st->stok_asal?></td>
            <td><?=$st->stok?></td>
            <td>
              <a href="<?=base_url("stok/edit/index/".$st->id)?>" class="btn btn-info"><i class="fa fa-edit"></i> Edit</a>
              <a href="<?=base_url("stok/hapus/index/".$st->id)?>" class="btn btn-danger btn-hapus" data-id="<?=$st->id?>"><i class="fa fa-trash"></i> Hapus</a>
            </td>
          </tr>
        <?php } ?>

      </tbody>
    </table>
  </div>
</div>
