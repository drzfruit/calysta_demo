//listener
$("#produk_table").off("click",".btn-hapus");
$("#produk_table").on("click",".btn-hapus",function(ev){
  ev.preventDefault();
  var c = confirm("Apakah anda yakin mas bro!");
  if(c){
    var id = $(this).attr("data-id");
    var apiurl = "<?=base_url()?>produk/api/hapus/"+encodeURIComponent(id);
    $.get(apiurl).done(function(dt){
      if(dt.status==200){
        alert("Berhasil!");
        window.location.reload();
      }else{
        alert("Gagal: "+dt.message);
      }
    }).fail(function(){
      alert("Error bray!");
    });
  }
});
