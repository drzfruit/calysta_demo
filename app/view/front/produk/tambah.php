<div class="row">
  <div class="col-md-12">
    <h4>Tambah produk</h4>
    <?php if(isset($notif)){ ?>
      <div class="alert alert-info">
        <p><?=$notif?></p>
      </div>
    <?php } ?>
    <form id="ikantor_form" action="<?=base_url('produk/tambah/index/')?>" method="post" class="form-horizontal">
      <div class="form-group">
        <div class="col-md-12">
          <label for="inama">Nama</label>
          <input id="inama" name="nama" class="form-control" value="" placeholder="Nama lengkap" />
        </div>
        <div class="col-md-12">
          <label for="iejenis">jenis</label>
          <input id="iejenis" name="jenis" class="form-control" value="" placeholder="jenis" />
        </div>
        <div class="col-md-12">
          <label for="iekategori">kategori</label>
          <input id="iekategori" name="kategori" class="form-control" value="" placeholder="kategori" />
        </div>
        <div class="col-md-12">
          <label for="ieharga">harga</label>
          <input id="ieharga" name="harga" class="form-control" value="" placeholder="harga" />
        </div>
      </div>


      <div class="form-action">
        <div class="col-md-12">
          <div class="btn-group pull-right">
            <a href="<?=base_url('produk')?>" class="btn btn-default"><i class="fa fa-chevron-left"></i> Kembali</a>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
