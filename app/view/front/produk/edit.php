<div class="row">
  <div class="col-md-12">
    <h4>Edit produk</h4>
    <?php if(isset($notif)){ ?>
      <div class="alert alert-info">
        <p><?=$notif?></p>
      </div>
    <?php } ?>
    <form id="ieproduk_form" action="<?=base_url('produk/edit/index/'.$produk->id)?>" method="post" class="form-horizontal">
      <div class="form-group">
        <div class="col-md-12">
          <label for="ieid">ID</label>
          <input id="id" name="id" class="form-control disabled" value="<?=$produk->id?>" disabled />
        </div>
        <div class="col-md-12">
          <label for="iejenis">jenis</label>
          <input id="iejenis" name="jenis" class="form-control" value="<?=$produk->jenis?>" />
        </div>
        <div class="col-md-12">
          <label for="iekategori">kategori</label>
          <input id="iekategori" name="kategori" class="form-control" value="<?=$produk->kategori?>" />
        </div>
        <div class="col-md-12">
          <label for="ienama">nama</label>
          <input id="ienama" name="nama" class="form-control" value="<?=$produk->nama?>" />
        </div>
        <div class="col-md-12">
          <label for="ieharga">haraga</label>
          <input id="ieharga" name="harga" class="form-control" value="<?=$produk->harga?>" />
        </div>
      </div>
      <div class="form-action">
        <div class="col-md-12">
          <div class="btn-group pull-right">
            <a href="<?=base_url('produk')?>" class="btn btn-default"><i class="fa fa-chevron-left"></i> Kembali</a>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan Perubahan</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
