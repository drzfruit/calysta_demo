<div class="row">
  <div class="col-md-6">
    <h4>Data produk</h4>
  </div>
  <div class="col-md-6">
    <div class="btn-group pull-right">
      <a href="<?=base_url("produk/tambah/index")?>" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Data</a>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <table id="produk_table" class="table">
      <thead>
        <tr>
          <th>Id</th>
          <th>jenis</th>
          <th>kategori</th>
          <th>nama</th>
          <th>harga</th>
        </tr>
      </thead>
      <tbody>

        <?php foreach($produk as $cp){ ?>
          <tr>
            <td><?=$cp->id?></td>
            <td><?=$cp->jenis?></td>
            <td><?=$cp->kategori?></td>
            <td><?=$cp->nama?></td>
            <td><?=$cp->harga?></td>
            <td>
              <a href="<?=base_url("produk/edit/index/".$cp->id)?>" class="btn btn-info"><i class="fa fa-edit"></i> Edit</a>
              <a href="<?=base_url("produk/hapus/index/".$cp->id)?>" class="btn btn-danger btn-hapus" data-id="<?=$cp->id?>"><i class="fa fa-trash"></i> Hapus</a>
            </td>
          </tr>
        <?php } ?>

      </tbody>
    </table>
  </div>
</div>
