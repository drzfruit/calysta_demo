<div class="row">
  <div class="col-md-12">
    <h4>Edit Siswa</h4>
    <?php if(isset($notif)){ ?>
      <div class="alert alert-info">
        <p><?=$notif?></p>
      </div>
    <?php } ?>
    <form id="iekantor_form" action="<?=base_url('kantor/edit/index/'.$kantor->id)?>" method="post" class="form-horizontal">
      <div class="form-group">
        <div class="col-md-12">
          <label for="ieid">ID</label>
          <input id="id" name="id" class="form-control disabled" value="<?=$kantor->id?>" disabled />
        </div>
        <div class="col-md-12">
          <label for="ienama">Nama</label>
          <input id="ienama" name="nama" class="form-control" value="<?=$kantor->nama?>" />
        </div>
        <div class="col-md-12">
          <label for="iekode">kode</label>
          <input id="iekode" name="kode" class="form-control" value="<?=$kantor->kode?>" />
        </div>
        <div class="col-md-12">
          <label for="iealamat">alamat</label>
          <input id="iealamat" name="alamat" class="form-control" value="<?=$kantor->alamat?>" />
        </div>
      </div>
      <div class="form-action">
        <div class="col-md-12">
          <div class="btn-group pull-right">
            <a href="<?=base_url('kantor')?>" class="btn btn-default"><i class="fa fa-chevron-left"></i> Kembali</a>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan Perubahan</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
