<div class="row">
  <div class="col-md-6">
    <h4>Data kantor</h4>
  </div>
  <div class="col-md-6">
    <div class="btn-group pull-right">
      <a href="<?=base_url("kantor/tambah/index")?>" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Data</a>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <table id="kantor_table" class="table">
      <thead>
        <tr>
          <th>Id</th>
          <th>Kode</th>
          <th>Nama</th>
          <th>Alamat</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>

      </tbody>
    </table>
  </div>
</div>
