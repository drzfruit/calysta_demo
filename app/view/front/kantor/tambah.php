<div class="row">
  <div class="col-md-12">
    <h4>Tambah Kantor</h4>
    <?php if(isset($notif)){ ?>
      <div class="alert alert-info">
        <p><?=$notif?></p>
      </div>
    <?php } ?>
    <form id="ikantor_form" action="<?=base_url('kantor/tambah/index/')?>" method="post" class="form-horizontal">
      <div class="form-group">
        <div class="col-md-12">
          <label for="iname">Nama</label>
          <input id="iname" name="nama" class="form-control" value="" placeholder="Nama lengkap" />
        </div>
        <div class="col-md-12">
          <label for="iekode">kode</label>
          <input id="iekode" name="kode" class="form-control" value="" placeholder="kode" />
        </div>
        <div class="col-md-12">
          <label for="iealamat">alamat</label>
          <input id="iealamat" name="alamat" class="form-control" value="" placeholder="alamat" />
        </div>
      </div>
      <div class="form-action">
        <div class="col-md-12">
          <div class="btn-group pull-right">
            <a href="<?=base_url('kantor')?>" class="btn btn-default"><i class="fa fa-chevron-left"></i> Kembali</a>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
