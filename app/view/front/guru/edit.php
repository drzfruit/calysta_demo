<div class="row">
  <div class="col-md-12">
    <h4>Edit Guru</h4>
    <?php if(isset($notif)){ ?>
      <div class="alert alert-info">
        <p><?=$notif?></p>
      </div>
    <?php } ?>
    <form id="ieguru_form" action="<?=base_url('guru/edit/index/'.$guru->nip)?>" method="post" class="form-horizontal">
      <div class="form-group">
        <div class="col-md-12">
          <label for="ienip">NIP</label>
          <input id="ienip" name="nip" class="form-control disabled" value="<?=$guru->nip?>" disabled />
        </div>
        <div class="col-md-12">
          <label for="iename">Nama</label>
          <input id="iename" name="nama" class="form-control" value="<?=$guru->nama?>" />
        </div>
        <div class="col-md-12">
          <label for="iepelajaran">Pelajaran</label>
          <input id="iepelajaran" name="pelajaran" class="form-control" value="<?=$guru->pelajaran?>" />
        </div>
      </div>
      <div class="form-action">
        <div class="col-md-12">
          <div class="btn-group pull-right">
            <a href="<?=base_url('guru')?>" class="btn btn-default"><i class="fa fa-chevron-left"></i> Kembali</a>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
