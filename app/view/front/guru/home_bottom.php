//listener
$("#guru_table").off("click",".btn-hapus");
$("#guru_table").on("click",".btn-hapus",function(ev){
  ev.preventDefault();
  var c = confirm("Apakah anda yakin mas bro!");
  if(c){
    var guru_id = $(this).attr("data-id");
    var apiurl = "<?=base_url()?>guru/api/hapus/"+encodeURIComponent(guru_id);
    $.get(apiurl).done(function(dt){
      if(dt.status==200){
        alert("Berhasil!");
        window.location.reload();
      }else{
        alert("Gagal: "+dt.message);
      }
    }).fail(function(){
      alert("Error bray!");
    });
  }
});
