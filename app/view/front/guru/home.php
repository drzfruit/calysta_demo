<div class="row">
  <div class="col-md-6">
    <h4>Data Guru</h4>
  </div>
  <div class="col-md-6">
    <div class="btn-group pull-right">
      <a href="<?=base_url("guru/tambah/index")?>" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Data</a>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <table id="guru_table" class="table">
      <thead>

        <tr>
          <th>NIP</th>
          <th>Nama</th>
          <th>Pelajaran</th>
          <th>action</th>
        </tr>
      </thead>
      <tbody>

        <?php foreach($guru as $gu){ ?>
          <tr>
            <td><?=$gu->nip?></td>
            <td><?=$gu->nama?></td>
            <td><?=$gu->pelajaran?></td>
            <td>
              <a href="<?=base_url("guru/edit/index/".$gu->nip)?>" class="btn btn-info"><i class="fa fa-edit"></i> Edit</a>
              <a href="<?=base_url("guru/hapus/index/".$gu->nip)?>" class="btn btn-danger btn-hapus"data-id="<?=$gu->nip?>"><i class="fa fa-trash"></i> Hapus</a>
            </td>
          </tr>
        <?php } ?>

      </tbody>
    </table>
  </div>
</div>
