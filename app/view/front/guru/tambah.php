<div class="row">
  <div class="col-md-12">
    <h4>Tambah Guru</h4>
    <?php if(isset($notif)){ ?>
      <div class="alert alert-info">
        <p><?=$notif?></p>
      </div>
    <?php } ?>
    <form id="guru_form" action="<?=base_url('guru/tambah/index/')?>" method="post" class="form-horizontal">
      <div class="form-group">
        <div class="col-md-12">
          <label for="inip">NIP</label>
          <input id="inip" name="nip" class="form-control" value="" placeholder="Nomor induk" />
        </div>
        <div class="col-md-12">
          <label for="iname">Nama</label>
          <input id="iname" name="nama" class="form-control" value="" placeholder="Nama lengkap" />
        </div>
        <div class="col-md-12">
          <label for="iepelajaran">pelajaran</label>
          <input id="iepelajaran" name="pelajaran" class="form-control" value="" placeholder="pelajaran" />
        </div>
      </div>
      <div class="form-action">
        <div class="col-md-12">
          <div class="btn-group pull-right">
            <a href="<?=base_url('guru')?>" class="btn btn-default"><i class="fa fa-chevron-left"></i> Kembali</a>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
